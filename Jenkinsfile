def label = "worker-${UUID.randomUUID().toString()}"
def DOCKER_REPO = "localhost:32002"

// ZAP
def ZAP_HOST = "jenkins-demo-zap"
def ZAP_PORT = 8080

podTemplate(label: label, containers: [
  containerTemplate(name: 'docker', image: 'docker', command: 'cat', ttyEnabled: true, envVars: [envVar(key: 'DOCKER_OPTS', value: '--insecure-registry 172.31.39.120:30006')]),
  containerTemplate(name: 'nodejs', image: 'brosenburgh/node10-puppeteer:1.0', command: 'cat', ttyEnabled: true),
  //containerTemplate(name: 'nodejs', image: 'node:10.16', command: 'cat', ttyEnabled: true)
],
volumes: [
  hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock'),
  persistentVolumeClaim(mountPath: '/root/owasp-cve-db', claimName: 'jenkins-demo', readOnly: false)
])
 {
  node(label) {
    sh "env"
    container("nodejs") {
      env.JAVA_HOME="${tool 'default-jdk'}"
      echo "${env.JAVA_HOME}"
      env.PATH="${env.JAVA_HOME}/bin:${env.PATH}"
      sh "export PATH=${env.JAVA_HOME}/bin:${env.PATH}"
      sh "java -version"

      stage('Checkout') {
          echo "checking out app"
          checkout scm
      }

      stage('Build') {
        stage('Install NPM dependencies') {
          sh "npm ci --quiet"
        }

        stage('Lint') {
          sh "npx ng lint"
        }

        stage('Build Service Libraries') {
          sh "npm run build.libraries"
        }
      }

      stage("Continuous Integration Tests") {

        parallel TestLibraries: {
          //TODO: why busted?
          // stage('Test Service Libraries') {
          //   sh "npm run test.libraries"
          // }
        },

        UnitTests: {
          stage('Unit Tests for App') {
            try {
              dir("codeveros-angular-ui") {
                sh "npm run test"
              }
            } catch (e) {
              throw e;
            } finally {
              publishHTML(target: [
                allowMissing         : false,
                alwaysLinkToLastBuild: true,
                keepAll              : true,
                reportDir            : 'reports',
                reportFiles          : 'coverage/lcov-report/index.html',
                reportName           : "Unit Tests"])
            }
          }
        }
      }


      stage("Static Code Analysis") {

        stage("3rd Party Library Analysis") {
          def dependencyCheck = tool 'dependency-check'
          sh "${dependencyCheck}/bin/dependency-check.sh --project codeveros-angular-ui --scan package-lock.json --format ALL --out . --data /root/owasp-cve-db"
          archiveArtifacts allowEmptyArchive: true, artifacts: 'dependency-check-report.*', onlyIfSuccessful: false
          dependencyCheckPublisher pattern: 'dependency-check-report.xml'
        }

        // parallel QualityScan: {
        stage("SonarQube Quality Analysis") {
          def scannerHome = tool 'sonarqube-scanner';
          withSonarQubeEnv('sonarqube') {
            sh "${scannerHome}/bin/sonar-scanner \
              -Dsonar.projectKey=codeveros-angular-ui \
              -Dsonar.sources=src,projects \
              -Dsonar.exclusions=src/test/** \
              -Dsonar.language=js \
              -Dsonar.typescript.lcov.reportPaths=reports/coverage/auth/lcov.info,reports/coverage/catalog/lcov.info,reports/coverage/codeveros-core-ui/lcov.info,reports/coverage/user/lcov.info \
              -Dsonar.dependencyCheck.htmlReportPath=dependency-check-report.html \
              -Dsonar.dependencyCheck.reportPath=dependency-check-report.xml"
          }
        }

        stage("Static Security Analysis") {
            /*
            Using tools such as Find Security Bugs or Fortify, scan your source code
            for potential insecure code.
            */
            try {
                // Run the security analysis goal
                //sh "mvn sonar:sonar"
                sh("./working.sh")
            } catch (err) {
                throw err
            }
        }
        // }
      }

      stage("Dynamic Code Analysis") {
        parallel OWASP: {
          //TODO: fix
          /*sh 'mkdir -p zap-proxy'
          sh "wget -q -O zap-proxy/report.html http://127.0.0.1:${ZAP_PORT}/OTHER/core/other/htmlreport"
          sh "wget -q -O zap-proxy/report.xml http://127.0.0.1:${ZAP_PORT}/OTHER/core/other/xmlreport"
          publishHTML([
             allowMissing         : false,
             alwaysLinkToLastBuild: true,
             keepAll              : true,
             reportDir            : 'zap-proxy',
             reportFiles          : 'report.html',
             reportName           : 'ZAP Proxy Report'
           ]) */
        },
        QualityScan: {

        }
      }

      stage('Prepare Build') {
        sh "npx ng build -c production"
      }
    } // End node container

    stage('Publish Docker Image') {
      container("docker") {
        docker.withRegistry("http://${DOCKER_REPO}", 'nexus') {
          stage('Build codeverosui') {
              echo "building docker image for codeverosui"
              def catalogImage = docker.build("${DOCKER_REPO}/codeverosui/codeverosui:latest")
              appImage = docker.build("${DOCKER_REPO}/codeverosui/codeverosui:latest")
              appImage.push()
          }
        }
      }
    }

    // stage('trigger deployment') {
    //    build 'deployment'
    // }

  }
}
