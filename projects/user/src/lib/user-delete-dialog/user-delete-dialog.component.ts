import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {User} from '../user.interface';

@Component({
  templateUrl: './user-delete-dialog.component.html',
  styleUrls: [ './user-delete-dialog.component.scss' ]
})
export class UserDeleteDialogComponent {
  user: User;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<UserDeleteDialogComponent>
  ) {
    if (data && data.user) {
      this.user = data.user;
    }
  }
}
