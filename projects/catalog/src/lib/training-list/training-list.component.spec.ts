import { TestBed, async } from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {TrainingListComponent} from './training-list.component';
import {TrainingModule} from '../training.module';
import {HttpClientModule} from '@angular/common/http';

describe('TrainingListComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ TrainingModule, HttpClientModule ]
    }).compileComponents();
  }));

  it('should create the training list', () => {
    const fixture = TestBed.createComponent(TrainingListComponent);
    const trainingList = fixture.debugElement.componentInstance;
    expect(trainingList).toBeTruthy();
  });

});
